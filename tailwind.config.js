const path = require('path');

module.exports = {
    prefix: '',
    purge: {
        enabled: process.env.NODE_ENV === 'production',
        content: [
            './src/**/*.{html,ts}',
        ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {},
    },
    corePlugins: {
        margin: true
    },
    plugins: [
        require(path.resolve(__dirname, ('src/styles/tailwind/icon-size'))),
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography')
    ],
};
