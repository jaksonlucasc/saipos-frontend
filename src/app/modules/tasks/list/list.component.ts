import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDrawer} from '@angular/material/sidenav';
import {Subject} from 'rxjs';
import {TasksService} from '../tasks.service';
import {takeUntil} from 'rxjs/operators';
import {Task} from '../tasks.types';
import {MatDialog} from '@angular/material/dialog';
import {TasksDialogSecurityComponent} from '../dialog-security/dialog-security.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'tasks-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TasksListComponent implements OnInit, OnDestroy
{
    @ViewChild('matDrawer', {static: true}) matDrawer: MatDrawer;

    tasks: Task[];
    tasksCount: any = {
        completed: 0,
        incomplete: 0,
        total: 0
    };

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _changeDetectorRef: ChangeDetectorRef,
        private _router: Router,
        private _tasksService: TasksService,
        private _dialog: MatDialog,
        private _snackBar: MatSnackBar
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Get the tasks
        this._tasksService.tasks$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tasks: Task[]) => {
                this.tasks = tasks;

                // Update the counts
                this.tasksCount.total = this.tasks.length;
                this.tasksCount.completed = this.tasks.filter(task => task.completed).length;
                this.tasksCount.incomplete = this.tasksCount.total - this.tasksCount.completed;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to task
     *
     * @param id
     */
    goToTask(id: number): void
    {
        // Get the current activated route
        let route = this._activatedRoute;
        while ( route.firstChild )
        {
            route = route.firstChild;
        }

        // Go to task
        this._router.navigate(['/tasks', id], {relativeTo: route});

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * On backdrop clicked
     */
    onBackdropClicked(): void
    {
        // Get the current activated route
        let route = this._activatedRoute;
        while ( route.firstChild )
        {
            route = route.firstChild;
        }

        // Go to the parent route
        this._router.navigate(['../'], {relativeTo: route});

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Toggle the completed status
     *
     * @param task
     */
    toggleCompleted(task: Task): void
    {
        if (task.completed) {
            const ref = this._dialog.open(TasksDialogSecurityComponent, {
                data: task
            });

            ref.afterClosed().subscribe((result) => {
                if (result) {
                    // Toggle the completed status
                    task.completed = !task.completed;

                    // Update task
                    this.updateTask(task);

                    return;
                }
            });

            return;
        }

        // Toggle the completed status
        task.completed = !task.completed;

        // Update task
        this.updateTask(task);
    }

    /**
     * Update task
     *
     * @param task
     */
    updateTask(task: Task): void
    {
        // Update the task on the server
        this._tasksService.update(task.id, task)
            .subscribe(() => {
                // Show snack message
                this._snackBar.open('Tarefa atualizada.', 'Fechar', {
                    duration: 3000
                });

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }
}
