export interface Task
{
    id: number;
    responsibleName: string;
    responsibleEmail: string;
    description: string;
    completed: boolean;
    createdAt: string;
    updatedAt: string;
}
