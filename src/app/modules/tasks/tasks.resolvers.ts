import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TasksService} from './tasks.service';
import {Task} from './tasks.types';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TasksResolver implements Resolve<any> {

    /**
     * Constructor
     */
    constructor(
        private _tasksService: TasksService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task[]>
    {
        return this._tasksService.getAll();
    }
}

@Injectable({
    providedIn: 'root'
})
export class TaskResolver implements Resolve<any> {

    /**
     * Constructor
     */
    constructor(
        private _tasksService: TasksService,
        private _router: Router
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task>
    {
        return this._tasksService.getById(parseInt(route.paramMap.get('id'), 10));
    }
}
