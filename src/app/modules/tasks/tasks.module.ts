import {NgModule} from '@angular/core';
import {TasksComponent} from './tasks.component';
import {TasksListComponent} from './list/list.component';
import {RouterModule} from '@angular/router';
import {tasksRoutes} from './tasks.routing';
import {SharedModule} from '../../shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TasksDetailsComponent} from './details/details.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TasksDialogSecurityComponent} from './dialog-security/dialog-security.component';

@NgModule({
    declarations: [
        TasksComponent,
        TasksListComponent,
        TasksDetailsComponent,
        TasksDialogSecurityComponent
    ],
    imports: [
        RouterModule.forChild(tasksRoutes),
        SharedModule,
        MatSidenavModule,
        MatButtonModule,
        MatTooltipModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        MatDialogModule,
        MatProgressSpinnerModule,
    ]
})
export class TasksModule {
}
