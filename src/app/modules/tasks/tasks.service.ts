import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Task} from './tasks.types';
import {environment} from '../../../environments/environment';
import {map, switchMap, take, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TasksService
{
    private _task: BehaviorSubject<Task | null> = new BehaviorSubject(null);
    private _tasks: BehaviorSubject<Task[] | null> = new BehaviorSubject([]);

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for task
     */
    get task$(): Observable<Task>
    {
        return this._task.asObservable();
    }

    /**
     * Getter for tasks
     */
    get tasks$(): Observable<Task[]>
    {
        return this._tasks.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get all the tasks
     */
    getAll(): Observable<Task[]>
    {
        return this._httpClient.get<Task[]>(`${environment.api}/tasks`).pipe(
            tap((tasks: Task[]) => {
                // Update tasks
                this._tasks.next(tasks);

                // Return tasks
                return tasks;
            })
        );
    }

    /**
     * Get task by id
     *
     * @param id
     */
    getById(id: number): Observable<Task>
    {
        return this._tasks.pipe(
            take(1),
            map((tasks) => {
                // Find the task
                const task = tasks.find(item => item.id === id) || null;

                // Update the task
                this._task.next(task);

                // Return the task
                return task;
            })
        );
    }

    /**
     * Create task
     *
     * @param task
     */
    create(task: Task): Observable<Task>
    {
        return this.tasks$.pipe(
            take(1),
            switchMap((tasks) => this._httpClient.post<Task>(`${environment.api}/tasks`, task).pipe(
                map((newTask) => {

                    // Update task with the new task
                    this._tasks.next([...tasks, newTask]);

                    // Update task
                    this._task.next(newTask);

                    // Return the new task
                    return newTask;
                })
            ))
        );
    }

    /**
     * Update task
     *
     * @param id
     * @param task
     */
    update(id: number, task: Task | any): Observable<Task>
    {
        return this.tasks$.pipe(
            take(1),
            switchMap(tasks => this._httpClient.patch<Task>(`${environment.api}/tasks/${id}`, task).pipe(
                map((updatedTask: Task) => {

                    // Find the index of the updated task
                    const index = tasks.findIndex(item => item.id === id);

                    // Update the task
                    tasks[index] = updatedTask;

                    // Update the tasks
                    this._tasks.next(tasks);

                    // Return the updated task
                    return updatedTask;
                })
            ))
        );
    }

    /**
     * Remove task
     *
     * @param id
     */
    remove(id: number): Observable<any>
    {
        return this.tasks$.pipe(
            take(1),
            switchMap(tasks => this._httpClient.delete(`${environment.api}/tasks/${id}`).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted tasks
                    const index = tasks.findIndex(item => item.id === id);

                    // Delete the task
                    tasks.splice(index, 1);

                    // Update the tasks
                    this._tasks.next(tasks);

                    // Return the deleted task
                    return isDeleted;
                })
            ))
        );
    }
}
