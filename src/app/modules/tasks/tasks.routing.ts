import {Route} from '@angular/router';
import {TasksComponent} from './tasks.component';
import {TasksListComponent} from './list/list.component';
import {TasksDetailsComponent} from './details/details.component';
import {CanDeactivateTasksDetails} from './tasks.guards';
import {TaskResolver, TasksResolver} from './tasks.resolvers';

export const tasksRoutes: Route[] = [
    {
        path: '',
        component: TasksComponent,
        children: [
            {
                path: '',
                component: TasksListComponent,
                resolve: {
                    tasks: TasksResolver
                },
                children: [
                    {
                        path: ':id',
                        component: TasksDetailsComponent,
                        resolve: {
                            task: TaskResolver
                        },
                        canDeactivate: [CanDeactivateTasksDetails]
                    }
                ]
            }
        ]
    }
];
