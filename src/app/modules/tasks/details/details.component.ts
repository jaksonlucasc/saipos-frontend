import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MatDrawerToggleResult} from '@angular/material/sidenav';
import {TasksListComponent} from '../list/list.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TasksService} from '../tasks.service';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Task} from '../tasks.types';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'tasks-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class TasksDetailsComponent implements OnInit, OnDestroy
{
    taskForm: FormGroup;
    task: Task;

    errorMessage: string;
    saving: boolean = false;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _tasksListComponent: TasksListComponent,
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _tasksService: TasksService,
        private _snackBar: MatSnackBar
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Open the drawer
        this._tasksListComponent.matDrawer.open();

        // Create the form
        this.taskForm = this._formBuilder.group({
            id: [null],
            responsibleName: ['', Validators.required],
            responsibleEmail: ['', Validators.required],
            description: ['', Validators.required]
        });

        // Get the task
        this._tasksService.task$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((task: Task) => {
                this.task = task;

                // Update form
                this.taskForm.patchValue(task);

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Close the drawer
     */
    closeDrawer(): Promise<MatDrawerToggleResult>
    {
        return this._tasksListComponent.matDrawer.close();
    }

    /**
     * On submit
     */
    onSubmit(): void
    {
        // Mark all touched
        this.taskForm.markAllAsTouched();

        // Check if form is valid
        if (this.taskForm.invalid) {
            return;
        }

        // Show loading
        this.saving = true;

        // Create or update
        (!this.taskForm.get('id').value ?
                this._tasksService.create(this.taskForm.getRawValue()) :
                this._tasksService.update(this.taskForm.get('id').value, this.taskForm.getRawValue())
        ).subscribe(
            () => {
                const message = this.taskForm.get('id').value ? 'Tarefa atualizada' : 'Tarefa criada';
                this._snackBar.open(message, 'Fechar', {
                    duration: 3000
                });

                // Hide loading
                this.saving = false;

                // Close drawer
                this._tasksListComponent.onBackdropClicked();

                // Mark for check
                this._changeDetectorRef.markForCheck();
                return;
            },
            (err) => {
                // Show the error
                this.errorMessage = err.error.message;

                // Hide loading
                this.saving = false;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }
}
