import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Task} from '../tasks.types';
import {TasksService} from '../tasks.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'tasks-dialog-security',
    templateUrl: './dialog-security.component.html',
    styleUrls: ['./dialog-security.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksDialogSecurityComponent implements OnInit
{
    task: Task;
    password: string;
    errorMessage: string;

    /**
     * Constructor
     */
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: Task,
        private _ref: MatDialogRef<TasksDialogSecurityComponent>,
        private _tasksService: TasksService,
        private _snackBar: MatSnackBar,
        private _changeDetectorRef: ChangeDetectorRef,
    ) {
        this.task = data;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    onSubmit(): void
    {
        // Toggle the completed status
        const body = {
            completed: !this.task.completed,
            supervisorPassword: this.password
        };

        // Update the task on the server
        this._tasksService.update(this.task.id, body)
            .subscribe(() => {
                // Show snack message
                this._snackBar.open('Tarefa atualizada.', 'Fechar', {
                    duration: 3000
                });

                return this._ref.close(true);
            }, (err) => {
                // Show the error
                this.errorMessage = err.error.message;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }
}
