import {Route} from '@angular/router';
import {LayoutComponent} from './layout/layout.component';

export const appRoutes: Route[] = [

    {
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full'
    },

    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'tasks',
                loadChildren: () => import('./modules/tasks/tasks.module').then(m => m.TasksModule)
            }
        ]
    }

];
