import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';

@NgModule({
    imports: [
        HttpClientModule
    ]
})
export class CoreModule
{
    /**
     * Constructor
     */
    constructor(
        private _domSanitizer: DomSanitizer,
        private _matIconRegistry: MatIconRegistry,
        @Optional() @SkipSelf() parentModule?: CoreModule
    )
    {
        this._matIconRegistry.addSvgIconSetInNamespace('awesome_regular', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/awesome-regular.svg'));
        this._matIconRegistry.addSvgIconSetInNamespace('awesome_solid', this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/awesome-solid.svg'));
    }
}
